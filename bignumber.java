import static java.lang.System.out;
import java.util.*;

public class BigNumber {

    Digit head; // reference to the head node
    Digit tail;  // reference to the tail node
    private String value;
    private long size; //reference to size of list

    public BigNumber() {
        //default constructor that initializes tail
        tail = new Digit();
        head = new Digit();
    }//end default constructor

    public BigNumber(String strNum) {

        String bigNum = strNum.substring(0, 1);
        if (bigNum.equals("-")) {
            head = new Digit(-1);
        } else {
            strNum = " " + strNum;
            head = new Digit(1);
        }
        for (int i = strNum.length() - 1; i >= 1; i--) {
            String value = strNum.substring(i, i + 1);
            Digit temp = new Digit(Integer.parseInt(value));
            addVal(temp);
        }
        updateTail();

    }//end string constructor

    public BigNumber(final BigNumber num) {

        // copy constructor, notice that you need to create a new list which represents
        // the same number as numthis(); //call to default constructor-initializes tail
        if (num.head.value == -1) {
            head = new Digit(-1);
        } else {
            head = new Digit(1);
        }

        Digit iter2 = num.tail;

        for (; iter2 != num.head; iter2 = iter2.prev) {
            Digit tmp = new Digit(iter2.value);
            addVal(tmp);
        }

        updateTail();
    }

    void addVal(Digit node) {
        node.next = head.next;
        head.next = node;
        node.prev = head;
        if (node.next != null) 
        {
            node.next.prev = node;
        }
        
       
    }//end addVal

    public long getSize() {
        size=0;
         for(Digit iter= this.head.next; iter!=null; iter=iter.next)
         {
             size++;
         }
         return size;
    }

    //the whole program goes to doo doo if we remove updateTail so just keep it
    //here
    public void updateTail() {
        Digit iter1 = head;
        for (; iter1 != null; iter1 = iter1.next) {
            tail = iter1;
            tail.value = iter1.value;
        }
    }//end updateTail

    public void insertEnd(int value) {
        Digit temp = new Digit(value);
        Digit iter=tail;
        Digit iter1=head;
        if (iter == null) {
            iter1 = iter = temp;
        } else {
           
            iter.next = temp;
            temp.prev = iter;
            iter = temp;
        }
        updateTail();
        size++;
    }//end insertEnd

    public boolean isLarger(BigNumber b) {
        if (this.getSize() > b.getSize()) {
            return true;
        }
        if (this.getSize() == b.getSize()) {
            Digit iter = this.head.next;
            Digit iter1 = b.head.next;
            while (iter != null && iter1 != null) {
                if (iter.value > iter1.value) {
                    return true;
                }
                if (iter.value < iter1.value) {
                    return false;
                } else {
                    iter = iter.next;
                    iter1 = iter1.next;
                }
            }
        }
        return false;
    }//end isLarger
    public boolean isEqual(BigNumber b)
    {
        Digit iter1=this.head.next;
        Digit iter2=b.head.next;
       
        if(this.getSize()==b.getSize())
        {
            for(;iter1!=null && iter2!=null; iter1=iter1.next, iter2=iter2.next)
            {
                if(iter1.value!=iter2.value)
                {
                    return false;
                }
                else
                {
                    return true;
                }
        }
        }

        return false;
    }
   public void delete(long index)
  {
     Digit curr=this.head;
     for(int i=0; i<index; i++)
     {
         curr=curr.next;
     }
     curr.next=curr.next.next;
  }
   public void deleteZero()
   {
      Digit iter=this.head;
      if(iter.value==1 || iter.value==-1)
      {
          
        for(Digit tempIter= this.head.next; tempIter!=null; tempIter=tempIter.next)
        {
            if(tempIter.value!=0)
            {
                break;
            }
            else
            {
                this.delete(0);
            }
        }
      }
      else
      {
        for(Digit tempIter= this.head; tempIter!=null; tempIter=tempIter.next)
        {
            if(tempIter.value!=0)
            {
                break;
            }
            else
            {
                this.delete(0);
            }
        }
      }
            
   }

    // addition assignment: adding the given number to the current number, +=
    public void add_assign(final BigNumber b) {
        int val = 0, temp = 0;
        Digit iter1 = this.tail;
        Digit iter2 = b.tail;

        while (iter1 != this.head || iter2 != b.head) {
            //this and b are the same size
            if (iter1 != this.head && iter2 != b.head) {
                val = (iter1.value + iter2.value + temp) % 10;
                temp = (iter1.value + iter2.value + temp) / 10;
                iter1.value = val;
                iter1 = iter1.prev;
                iter2 = iter2.prev;
            } else if (iter1 == this.head && iter2 != b.head) {
                val = (iter2.value + temp) % 10;
                temp = (iter2.value + temp) / 10;
                iter2 = iter2.prev;
                //this.printList();
                Digit carry = new Digit(val);
                this.addVal(carry);
            } else if (iter1 != this.head && iter2 == b.head) {
                val = (iter1.value + temp) % 10;
                temp = (iter1.value + temp) / 10;
                iter1.value = val;
                iter1 = iter1.prev;
            }//end if
        }//end while
        if (temp != 0) {
            Digit tmp = new Digit(temp);
            this.addVal(tmp);
        }
    }//end add_assign
//

    public void sub_assign(final BigNumber b) {
        int val = 0, temp = 0;
        Digit iter1 = this.tail;
        Digit iter2 = b.tail;
      
        while (iter1 != this.head || iter2 != b.head) {
            if(iter2==b.head && iter2.value==0 )
            {
                break;
            }
            else if(iter1==this.head && iter1.value==1)
            {
                break;
            }
            if (iter1 != this.head && iter2 != b.head) {
                if ((iter1.value) + temp >= (iter2.value)) {
                    val = iter1.value + temp - iter2.value;
                    temp = 0;
                } else {
                    val = (iter1.value + temp + 10) - iter2.value;
                    temp = -1;
                }
                iter1.value = val;
                iter1 = iter1.prev;
                iter2 = iter2.prev;
               
            }
            while (iter1 != this.head && iter2 == b.head) {
                if (iter1.value >= 1) {
                    val = iter1.value + temp;
                    temp = 0;
                } else {
                    if (temp != 0) {
                        val = iter1.value + 10 + temp;
                        temp = -1;
                    } else {
                        val = iter1.value;
                    }
                     
                }
                iter1.value = val;
                iter1 = iter1.prev;

            }
        }
    }//end sub_assign

    public void mult_assign(final BigNumber b) {
       BigNumber finalResult = new BigNumber("0");
       BigNumber t= new BigNumber(this);
       finalResult.updateTail();
       int digitNum = 0;
        for(Digit biter = b.tail; biter != b.head; biter = biter.prev) {
           BigNumber res = singleMult(new BigNumber(this), biter.value);
           res.updateTail();
           for(int i =0; i != digitNum; i++ ){
               res.insertEnd(0);
           }
           finalResult.add_assign(res);
           digitNum++;
       }
        finalResult.deleteZero();
//        finalResult.printList();
        this.add_assign(finalResult);
        this.deleteZero();
//        out.println("after add");
//        this.printList();
        this.sub_assign(t);
       
    }//end mult_assign
    
    public static BigNumber singleMult(BigNumber a,int b) {
    BigNumber result = new BigNumber();
    //somehow result needs the head value (sign of current value) assigned to it 
    result.head=new Digit(1); //arbitrary for mean time
    int val = 0, tmp = 0;
    for (Digit aiter = a.tail; aiter != a.head; aiter = aiter.prev) {
        val = (aiter.value * b + tmp) % 10;
//        out.print(val);
        tmp = (aiter.value * b + tmp) / 10;
        Digit prod = new Digit(val);
        result.addVal(prod);
//        out.println("here");
    }//end for
    if (tmp != 0) {
        Digit carry = new Digit(tmp);
        result.addVal(carry);
    }
    //result.printList();
    return result;
    }//end singleMult
//a
    public void div_assign(final BigNumber b) {
     BigNumber temp= new BigNumber(this);
        BigNumber temp2=new BigNumber();
        temp2.head=new Digit(1);
        String bVal="";
        long val=0;
        long result;
        int count=0;
        for(Digit iter1= b.head.next; iter1!=null; iter1=iter1.next)
         {
             long value=iter1.value;
             bVal+=Long.toString(value);
         } //converts b into long value
        long b_Val=Long.parseLong(bVal);
//        if(b_Val==0)
//        {
//            out.println("Division by zero not supported");
//            System.exit(0);
//
//        }
        Digit iter1=temp.head.next;
        val+=iter1.value;
        Stack stack= new Stack();
        while(iter1.next!=null)
        {
            while(val<b_Val)
            {


                if(iter1.next==null)
                {
                    break;
                }
                val*= 10;
                val+=iter1.next.value;
                iter1=iter1.next;
               
                if(val%b_Val==val)
                {
                    stack.push(0);
                }
            }

            result=val/b_Val;
            stack.push(result);
            val= val-(b_Val* result);
           

        }
        //reverses the list
        while(!stack.isEmpty())
        {
            temp2.addVal(new Digit(Integer.parseInt(stack.pop().toString())));
        }

        temp2.deleteZero();
        temp2.updateTail();
        this.add_assign(temp2);
        this.sub_assign(temp);
        this.deleteZero();
           
        
    }//end div_assign

    public void mod_assign(final BigNumber b) {
        BigNumber temp = new BigNumber(this);
        BigNumber temp2= new BigNumber(b);
        temp.div_assign(temp2);
        BigNumber temp3=new BigNumber(temp);
        temp3.mult_assign(temp2);
        temp3.deleteZero();
        this.sub_assign(temp3);
        this.deleteZero();
        

    }//end mod_assign
     public void ex_assign(int counter)
     {
         BigNumber temp= new BigNumber(this);
         for(int i=1; i<counter; i++)
         {
             this.mult_assign(temp);
             this.deleteZero();
         } 
     }
     
    // addition: return a + b
    public static BigNumber add(final BigNumber a, final BigNumber b) {
        Digit iter1=a.head;
        Digit iter2=b.head;
        BigNumber c = new BigNumber();;
        if(iter1.value==1 && iter2.value==-1)
        {
          if(a.isLarger(b))
          { 
               c= new BigNumber(a);
               c.sub_assign(b);
               
           } 
          else if(!a.isLarger(b) && !a.isEqual(b))
          {
              c= new BigNumber(b);
              c.sub_assign(a);            
          }
          else if(a.isEqual(b))
          {
              c=new BigNumber("0");
          }
        }
        else if(iter1.value==-1 && iter2.value==1)
        {
            if(a.isLarger(b))
            {
                c= new BigNumber(a);
                c.head.value = 1;
                c.sub_assign(b);
                c.head.value = -1;
            }
            if(!a.isLarger(b) && !a.isEqual(b))
            {
                c= new BigNumber(b);
                c.sub_assign(a);
            }
            else if(a.isEqual(b))
            {
                c=new BigNumber("0");
            }
        }
        else
        {
            c= new BigNumber(a);
            c.add_assign(b);
        }
            return c;
    }//end add

    public static BigNumber sub(final BigNumber a, final BigNumber b) {
        //Note: I'm not sure we need the commmented out statements, but they're there
        //if you digress
        Digit iter1=a.head;
        Digit iter2=b.head;
        BigNumber c = new BigNumber();
        BigNumber d = new BigNumber();
        if(iter1.value==1 && iter2.value==-1)
        {
            c= new BigNumber(b);
            c.head.value=1;
            c.add_assign(a);
        }
        else if(iter1.value==-1 && iter2.value==1)
        {
            c= new BigNumber(a);
            c.head.value=1;
            c.add_assign(b);
            c.head.value = -1;
        }
        else if(iter1.value==iter2.value)
        {
            if(a.isLarger(b) && iter1.value==1 && iter2.value==1)
            {
               //should do nothing, but having no statement causes problems
               c=new BigNumber(a);
               c.sub_assign(b);
            }
           else if(a.isLarger(b) && iter1.value==-1 && iter2.value==-1)
            {
                c= new BigNumber(a);
                d= new BigNumber(b);
                c.head.value=1;
                d.head.value=1;
                c.sub_assign(b);
                c.head.value=-1;
            }
           else if(!a.isLarger(b) && iter1.value==1 && iter2.value==1)
            {
                c=new BigNumber(b);
                c.sub_assign(a);
                c.head.value=-1;
            }
           else if(!a.isLarger(b) && iter1.value==-1 && iter2.value==-1)
            {
                c=new BigNumber(b);
                d=new BigNumber(a);
                c.head.value = 1;
                d.head.value = 1;
                c.sub_assign(d);
            }
            else
            {
                c=new BigNumber("0");
                c.add_assign(c);
            }
        }
        else
        {
            //should do nothing, but having no statement causes problems
           c=new BigNumber(a);
        }
        return c;
      }//end sub

    public static BigNumber mult(final BigNumber a, final BigNumber b) {
    	 Digit iter1=a.head;
         Digit iter2=b.head;
         BigNumber c=new BigNumber();
         //c.mult_assign(b);
         if(iter1.value==iter2.value)
         {
             c= new BigNumber(a);
             c.mult_assign(b);
             c.head.value=1;
         }
         else if(!(iter1.value==iter2.value))
         {
             c=new BigNumber(a);
             c.mult_assign(b);
             c.head.value=-1;
         }
         else {
        	 c = new BigNumber("0");
         }
         return c;
    }//end mult
   

    public static BigNumber div(final BigNumber a, final BigNumber b) {
        Digit iter1=a.head;
        Digit iter2=b.head;
        BigNumber c;
        if(b.isLarger(a) && !b.isEqual(a))
        {
            return new BigNumber("0");
        }
        if(iter1.value==iter2.value)
        {
            c= new BigNumber(a);
            c.div_assign(b);
            c.head.value=1;
        }
        else if(iter1.value==-1 && iter2.value==1) {
        	c=new BigNumber(a);
            c.div_assign(b);
            c.head.value=-1;
            c.deleteZero();
        }
        else
        {
            c=new BigNumber(a);
            c.div_assign(b);
            c.head.value=-1;
        }
        return c;
    }//end div

    public static BigNumber mod(final BigNumber a, final BigNumber b) {
    	BigNumber c=new BigNumber();
        BigNumber d=new BigNumber();
        //BigNumber test= BigNumber.div(a,b);
        //BigNumber temp2= new BigNumber(test);
        //test.mult_assign(b);
        //test.sub_assign(test);
        if(b.isLarger(a) && !b.isEqual(a))
        {
            return new BigNumber("0");
        }
        /*this is testing if there's no remainder from div, then mod is 0
       else if(test.head.next.value==0)
        {
            out.println("No remainder. Result from division ");
            return temp2;
        }
        */
        else
        {
            c=new BigNumber(a);
            d=new BigNumber(b);
            c.head.value = 1;
            d.head.value = 1;
            c.mod_assign(d);
        }
       
        return c;
    }//end mod
    public static BigNumber expo(final BigNumber a, int b)
    {
        BigNumber c= new BigNumber(a);
        c.deleteZero();
        c.ex_assign(b);
        return c;
    }
    

    public void printList() {
      /*
        we can add a way to put commas but the easiest way would be to traverse 
        the list backwards... in other words, not very practical 
        */
        for (Digit iter = head; iter != null; iter = iter.next) {
            System.out.print(iter.value);
        }
        out.println();
    }//end printList
    public String toString()
    {
        String val="";
        for(Digit iter=this.head.next; iter!=null; iter=iter.next)
        {
            val+=iter.value;
        }
        return val;
    }
}//end class
