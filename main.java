import java.io.*;
import static java.lang.System.out;
public class Test_Method {

	public static void main(String[] args) {
            String number1 = "4333333333333333333333333333333330913903390832";
            String number2= "3213495985698498";
        
            BigNumber num1 = new BigNumber(number1);
            BigNumber num2 = new BigNumber(number2);
        
            
            
            // division
            System.out.println("Division");
            BigNumber.div(num1, num2).printList();
            System.out.println("=======================");
            
            
            // modulus 
            System.out.println("Division");
            BigNumber.mod(num1, num2).printList();
            System.out.println("=======================");
            
            // multiplication
            System.out.println("Multiplication");
            BigNumber.mult(num1, num2).printList();
            System.out.println("=======================");
            
            
            // subtraction
            System.out.println("Subtraction");
            BigNumber.add(num1, num2).printList();
            System.out.println("=======================");
            
            
            // addition
            System.out.println("Addition");
            BigNumber.sub(num1, num2).printList();
            System.out.println("=======================");
                
	}
}
