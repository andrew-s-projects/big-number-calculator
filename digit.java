import java.util.*;
class Digit{
	int value;    // data field
	Digit prev;
	Digit next;
        
        
	Digit(){
		value = 0;
		next = null;
		prev=null;
	}

	// initialize name by the value
	Digit(final int value){
		this.value = new Integer(value); //deep copy
		next = null;
		prev=null;
	}

	// copy constructor
	Digit(final Digit node){
		// put your code here
		value = new Integer(node.value); //deep copy
		next = node.next;
	}
	

}